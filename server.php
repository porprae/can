<?php

require_once "lib/nusoap.php";

function checkNotification($author_id){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "SELECT COUNT(log.log_id) as NumOfUnread
			  FROM log,post
			  WHERE log.noti_status = 0 AND
			  log.post_id = post.post_id AND
			  post.author_id =".$author_id;
	$result = mysqli_query($con,$query);
	
	while($row = mysqli_fetch_array($result)){
	  $count .= $row['NumOfUnread'];
	}
	
	$link = "<a href='#' onclick=\"getNotification()\">Notification(".$count.")</a>";
	mysqli_close($con);
	
	return $link;
}

function getNotification($author_id){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "SELECT log.log_id,log.post_id,post.title,user.username,log.timestamp,log.noti_status
			  FROM log,post,user 
			  WHERE post.author_id = ".$author_id." AND
					post.post_id = log.post_id AND
					log.commentator_id = user.user_id
			  ORDER BY log_id DESC";
	$result = mysqli_query($con,$query);

	$notification = "";
	while($row = mysqli_fetch_array($result)){
		$color="red";
		if($row['noti_status']==1) $color="black";
		$notification .= "<a href='#'><font color='".$color."'>log#<i>" . $row['log_id'] . "</i> ".
						 "<i>" . $row['username'] . "</i> ".
						 "comment on (post id <i>" . $row['post_id'] . ")</i> ".
						 "<i>" . $row['title'] . "</i> ".
						 "on <i>" . $row['timestamp'] . "</i></font></a><br> ";
	}
	
	updateNotiStatus($author_id);
	mysqli_close($con);
	
	return $notification;
}

function updateNotiStatus($author_id){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$sub_query = "SELECT post_id FROM post WHERE post.author_id =".$author_id;
	
	$query = "UPDATE log SET noti_status = 1
			  WHERE post_id IN (".$sub_query.") AND
			  noti_status = 0";
	mysqli_query($con,$query);
	
	mysqli_close($con);
}

function displayByPostId($post_id,$cur_user){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "SELECT * FROM log WHERE post_id=".$post_id." ORDER BY log_id DESC";
	$result = mysqli_query($con,$query);

	$table = "<table border='1'>
			<tr>
			<th>log_id</th>
			<th>post_id</th>
			<th>paragraph_id</th>
			<th>commentator_id</th>
			<th>comment</th>
			<th>timestamp</th>
			</tr>";

	while($row = mysqli_fetch_array($result)){
	  $table .= "<tr>";
	  $table .= "<td>" . $row['log_id'] . "</td>";
	  $table .= "<td>" . $row['post_id'] . "</td>";
	  $table .= "<td>" . $row['paragraph_id'] . "</td>";
	  $table .= "<td>" . $row['commentator_id'] . "</td>";
	  $table .= "<td>" . $row['comment'] . "</td>";
	  $table .= "<td>" . $row['timestamp'] . "</td>";
	  if($cur_user==$row['commentator_id']){
		$editButton = "<td><input type=button value=\"Edit\" onclick=\"showEditField(".$row['log_id'].",".$row['post_id'].")\" ></td>";
		$delButton = "<td><input type=button value=\"Delete\" onclick=\"delComment(".$row['log_id'].",".$row['post_id'].")\" ></td>";
		$table .= $editButton.$delButton;
	  }
	  else $table .= "</tr>";
	  }
	$table .= "</table>";
	
	mysqli_close($con);
	
	return $table;
}

function displayByParagraphId($post_id,$cur_user,$paragraph_id){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "SELECT * FROM log WHERE post_id=".$post_id." AND paragraph_id=".$paragraph_id." ORDER BY log_id DESC";
	$result = mysqli_query($con,$query);

	$table = "<table border='1'>
			<tr>
			<th>log_id</th>
			<th>post_id</th>
			<th>paragraph_id</th>
			<th>commentator_id</th>
			<th>comment</th>
			<th>timestamp</th>
			</tr>";

	while($row = mysqli_fetch_array($result)){
	  $table .= "<tr>";
	  $table .= "<td>" . $row['log_id'] . "</td>";
	  $table .= "<td>" . $row['post_id'] . "</td>";
	  $table .= "<td>" . $row['paragraph_id'] . "</td>";
	  $table .= "<td>" . $row['commentator_id'] . "</td>";
	  $table .= "<td>" . $row['comment'] . "</td>";
	  $table .= "<td>" . $row['timestamp'] . "</td>";
	  $table .= "</tr>";
	  }
	$table .= "</table>";
	
	mysqli_close($con);
	
	return $table;
}

function showEditField($log_id,$post_id,$cur_user){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "SELECT * FROM log WHERE post_id=".$post_id." ORDER BY log_id DESC";
	$result = mysqli_query($con,$query);

	$table = "<table border='1'>
			<tr>
			<th>log_id</th>
			<th>post_id</th>
			<th>paragraph_id</th>
			<th>commentator_id</th>
			<th>comment</th>
			<th>timestamp</th>
			</tr>";

	while($row = mysqli_fetch_array($result)){
		if($row['log_id']==$log_id){
		  $table .= "<tr>";
		  $table .= "<td>" . $row['log_id'] . "</td>";
		  $table .= "<td>" . $row['post_id'] . "</td>";
		  $table .= "<td>" . $row['paragraph_id'] . "</td>";
		  $table .= "<td>" . $row['commentator_id'] . "</td>";
		  $table .= "<td><input type=text id=\"new_comment\" /></td>";
		  $table .= "<td>" . $row['timestamp'] . "</td>";
		  if($cur_user==$row['commentator_id']){
			  $saveButton = "<td><input type=button value=\"Save\" onclick=\"editComment(".$row['log_id'].",".$row['post_id'].",new_comment.value,".$cur_user.")\" ></td>";
			  $cancelButton = "<td><input type=button value=\"Cancel\" onclick=\"displayByPostId()\" ></td>";
			  $table .= $saveButton.$cancelButton;
		  }
		  else $table .= "</tr>";
		 }
		 else{
		  $table .= "<tr>";
		  $table .= "<td>" . $row['log_id'] . "</td>";
		  $table .= "<td>" . $row['post_id'] . "</td>";
		  $table .= "<td>" . $row['paragraph_id'] . "</td>";
		  $table .= "<td>" . $row['commentator_id'] . "</td>";
		  $table .= "<td>" . $row['comment'] . "</td>";
		  $table .= "<td>" . $row['timestamp'] . "</td>";
		  if($cur_user==$row['commentator_id']){
			  $editButton = "<td><input type=button value=\"Edit\" onclick=\"showEditField(".$row['log_id'].",".$row['post_id'].",".$cur_user.")\" ></td>";
			  $delButton = "<td><input type=button value=\"Delete\" onclick=\"delComment(".$row['log_id'].",".$row['post_id'].",".$cur_user.")\" ></td>";
			  $table .= $editButton.$delButton;
		  }
		  else $table .= "</tr>";
		}
	  }
	$table .= "</table>";
	
	mysqli_close($con);
	
	return $table;
}

function addComment($post_id,$paragraph_id,$commentator_id,$comment){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "INSERT INTO log(post_id,paragraph_id,commentator_id,comment,timestamp,noti_status)
				VALUES(".$post_id.",".$paragraph_id.",".$commentator_id.",'".$comment."',now(),0)";
	$result = mysqli_query($con,$query);
	
	mysqli_close($con);
}

function editComment($log_id,$new_comment){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "UPDATE log SET comment ='".$new_comment."' ".
			 "WHERE log_id =".$log_id;
	mysqli_query($con,$query);
	
	mysqli_close($con);
}

function delComment($log_id){
	$con=mysqli_connect("localhost","root","root","thinkbox");
	// Check connection
	if (mysqli_connect_errno()){
	  return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$query = "DELETE FROM log
			  WHERE log_id =".$log_id;
	mysqli_query($con,$query);
	
	mysqli_close($con);
	
	return $log_id;
}

// create object to deal with service provider
$server = new soap_server();
$server->register("addComment");
$server->register("editComment");
$server->register("showEditField");
$server->register("delComment");
$server->register("displayByPostId");
$server->register("displayByParagraphId");
$server->register("getNotification");
$server->register("checkNotification");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>