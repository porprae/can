<?php

require_once "lib/nusoap.php";

$client = new soapclient("http://localhost/CAN/server.php",false);

$error = $client->getError();

if($error){
	echo "<h2>Constructor error</h2><pre>".$error."</pre>";
}

if($_GET['command'] == "displayByPostId") {
	$post_id = $_GET['post_id'];
	$cur_user = $_GET['cur_user'];
	$result = $client->call("displayByPostId",array("post_id"=>$post_id,"cur_user"=>$cur_user));
}

if($_GET['command'] == "displayByParagraphId") {
	$post_id = $_GET['post_id'];
	$cur_user = $_GET['cur_user'];
	$paragraph_id = $_GET['paragraph_id'];
	$result = $client->call("displayByParagraphId",array("post_id"=>$post_id,"cur_user"=>$cur_user,"paragraph_id"=>$paragraph_id));
}

if($_GET['command'] == "addComment") {
	$post_id = $_GET['post_id'];
	$paragraph_id = $_GET['paragraph_id'];
	$commentator_id = $_GET['commentator_id'];
	$comment = $_GET['comment'];
	$cur_user = $_GET['cur_user'];
	$client->call("addComment",array("post_id"=>$post_id,"paragraph_id"=>$paragraph_id,"commentator_id"=>$commentator_id,"comment"=>$comment));
	if($paragraph_id == 0) $result = $client->call("displayByPostId",array("post_id"=>$post_id,"cur_user"=>$cur_user));
}

if($_GET['command'] == "editComment") {
	$log_id = $_GET['log_id'];
	$post_id = $_GET['post_id'];
	$new_comment = $_GET['new_comment'];
	$cur_user = $_GET['cur_user'];
	$client->call("editComment",array("log_id"=>$log_id,"new_comment"=>$new_comment));
	$result = $client->call("displayByPostId",array("post_id"=>$post_id,"cur_user"=>$cur_user));
}

if($_GET['command'] == "showEditField") {
	$log_id = $_GET['log_id'];
	$post_id = $_GET['post_id'];
	$cur_user = $_GET['cur_user'];
	$result = $client->call("showEditField",array("log_id"=>$log_id,"post_id"=>$post_id,"cur_user"=>$cur_user));
}

if($_GET['command'] == "delComment") {
	$log_id = $_GET['log_id'];
	$post_id = $_GET['post_id'];
	$cur_user = $_GET['cur_user'];
	$client->call("delComment",array("log_id"=>$log_id));
	$result = $client->call("displayByPostId",array("post_id"=>$post_id,"cur_user"=>$cur_user));
}


if($_GET['command'] == "checkNotification") {
	$author_id = $_GET['author_id'];
	$result = $client->call("checkNotification",array("author_id"=>$author_id));
}

if($_GET['command'] == "getNotification") {
	$author_id = $_GET['author_id'];
	$result = $client->call("getNotification",array("author_id"=>$author_id));
}

$error2 = $client->getError();

if($error2){
	echo "<h2>Error</h2><pre>".$error2."</pre>";
}
else{
	echo $result;
}
?>